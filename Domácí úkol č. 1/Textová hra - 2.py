import time  # knihovna pro čas, chtěl jsem pozastavení na určitý čas
import textwrap  # zarovnání textu

###---------------------------------------------------------------------------------------------------------------------
#PS: Vymazal jsem to global... :)
# Ale níže jsem užíval globals(), abych dostal hodnotu, která se skrývá pod danou proměnnou, do jiné proměnné


choice = 0
behaviour = []  # načítá z points podle zvolené možnosti ve f-ci ifs()
width = 79  # šířka stránky pro zarovnávání textu

# --------------------------------------PŘEHLED FUNKCÍ------------------------------------------------------------------


def next():
    input("Pro pokračování stiskněte enter".center(width))


def input_correct(a, b, c, d, e, choice):
    while choice not in [a, b, c, d, e]:
        choice = input("Zadejte prosím svoji odpověď ve tvaru \"A\" nebo \"B\" atd.: ")
        continue


def ifs(answer_1, answer_2, comment_1, comment_2, comment_3, choice):               #kladení otázek
    if choice == answer_1:
        print(comment_1)
        behaviour.append(points[i][0])         #vkládá do behaviour daný počet bodů podle pozice, odvozené od čísla otázky (i)
    elif choice == answer_2:
        print(comment_2)
        behaviour.append(points[i][1])
    else:
        print(comment_3)
        behaviour.append(points[i][2])


def ifs_in(x, y, z, choice):                                                    #speciální případ kladení otázek č. 1 pro muže
    if choice == x:
        return "Na vrcholnou akci s oblekem nezazáříte. V pozvánce bylo Black-Tie = smoking."
    elif choice == y:
        return "Budete vypadat staromódně. Navíc žaket je denní formální oděv. Hned na začátku takové faux pas!"
    elif choice == z:
        return "Dobrá vzal jste si nejexkluzivnější typ pánského oděvu, ale budete vypadat staromódně."


# --------------------------------------DATA----------------------------------------------------------------------------
wellcome = ["Pracujete v nadnárodní společnosti působící v oblasti finančnictví. Jste pravou\nrukou nejvyššího šéfa"
            " alias majitele. Dostala jste od něj pozvánku pro Vás\na Vašeho partnera na Ples v opeře v Praze jako "
            "poděkování za dosavadní práci.\nPles v opeře je ale vrcholná společenská akce, kde si otestujete své "
            "znalosti\na dovednosti, jimiž disponujete v etiketě. Zvládnete to bez újmy?",
            "Pracujete v nadnárodní společnosti působící v oblasti finančnictví. Jste pravou\nrukou nejvyššího šéfa"
            " alias majitele. Dostal jste od něj pozvánku pro Vás\na Vaši partnerku na Ples v opeře v Praze jako "
            "poděkování za dosavadní práci.\nV ní Vás žádají, abyste přišel oblečen v tzv. Black-Tie. Ples v opeře je\n"
            "ale vrcholná společenská akce, kde si otestujete své znalosti\na dovednosti, jimiž disponujete v etiketě. "
            "Zvládnete to bez újmy?"]

questions_1 = ["I| Co si vyberete pro tuto sváteční příležitost ze svého šatníku?",
               "II| A co si oblečete na své ladné nožky?",
               "III| Vcházíte do honosného sálu. Okolo sebe potkáváte nerůznější slavné osobnosti.\nV tom zahlédnete svého "
               "šéfa, a tak mu jdete představit svou drahou polovičku.",
               "IV |Nyní je čas na slavnostní večeři.\n\nNa stole je bohatě prostřeno. Bude se podávat exkluzivní menu:"
               "\n\tpředkrm:\t\t\tletní salát s grilovaným melounem\n\tpolévka:\t\t\tkarotkový krém s jablky a zázvorem"
               "\n\thlavní jídlo I:\t\tlosos sous-vide s bramborovocelerovým pyré\n\thlavní jídlo II:\tkachní prsíčka na"
               " medu se zeleninou\n\tdezert:\t\t\t\tčokoládový fondant s lístky růže\n",
               "V| Konečně je čas na zábavu. Ale pozor, pravidla etikety platí i zde!\n\n"
               "Co se například stane, když k Vám přijde nepohledný muž Vás poprosit o tanec a Vy odmítnete?"]  # sada otázek ženy 0-4
questions_2 = ["I| Co si vyberete pro tuto sváteční příležitost ze svého šatníku?",
               "II| Cesta proběhla v pořádku. Nyní přicházíte do šatny, co uděláte?",
               questions_1[2], questions_1[3],
               "V| Co kupříkladu musíte udělat, když chcete vyzvat k tanci manželku Vašeho šéfa?"]  # sada otázek muži 0-4

sub_qns_1 = ["A - večerní róbu\nB - koktejlky\nC - minisukni\nD - rifle\nnapište: ",

             "A - silonky se sandálky\nB - silonky s lodičkami\nC - sandálky bez silonek "
             "\nD - lodičky bez silonek\nnapište: ",

             "Kdo se představí komu jako DRUHÝ?\nA - Váš muž se představí ženě od šéfa.\nB - Vašeho muže představíte "
             "šéfovi.\nC - Šéf vám představí svou ženu.\nD - Šéf se představí Vašemu muži.\nnapište: ",

             "Který příbor použijete na předkrm?\nLeží zde zleva od talíře: malá vidlička a dvě velké vidličky\n"
             "vpravo od talíře: ostrý nůž, symetrický neostrý nůž a lžíce,\nnad talířem leží napříč vidlička.\nVolby:\n"
             "A - malá vidlička vlevo od talíře\nB - velká vidlička dál od talíře\nC - velká vidlička u talíře\nD - "
             "vidlička napříč nad talířem.\nnapište: ",

             "A - Odmítnutí je naprosto nepřípustné!\nB - Nic. A pokud chcete, najdete si k tanci jiného partnera.\n"
             "C - Pokud je muž výrazně mladší než Vy, tak se nic nestane. Jinak jde o faux pas.\nD - Nic. "
             "Jen nesmíte s kýmkoliv jiným tančit následující tanec.\nnapište: "]  # sada doplňujících otázek ženy 0-4
sub_qns_2 = ["A - smoking\nB - oblek\nC - žaket\nD - rifle\nE - frak\nnapište: ",

             "A - Necháte si na sobě svršky a půjdete do sálu.\nB - Sundáte svršky nejdříve sobě, pak pomůžete dámě.\nC "
             "- Pomůžete sundat svršky dámě, pak vysvlečete své.\nD - Odhodíte své svršky na zem, neboť šatnářky je pak "
             "posbírají.\nnapište: ",

             "Kdo se představí komu jako DRUHÝ?\nA - Vaše žena se představí ženě od šéfa.\nB - Šéf se představí Vaší "
             "ženě.\nC - Vaši manželku představíte šéfovi.\nD - Šéf vám představí svou ženu.\nnapište: ",

             sub_qns_1[3],  # stejná jako u sub_qns_1[3]

             "A - Požádáte taktně frází \"Smím prosit?\" šéfovu choť.\nB - Chytnete taktně šéfovu choť za pravou ruku "
             "a odvedete ji na parket.\nC - Nejprve požádáte o svolení šéfa, pak teprve jeho manželky.\nD - Políbíte "
             "lehce choť na čelo a požádáte ji o tanec.\nnapište: "
             ]  # sada doplňujících otázek muži 0-4

answers_1 = [["A", "B"], ["B", "A"], ["C", None], ["A", None], ["D", None]]
answers_2 = [["A", "D"], ["C", "B"], ["B", None], ["A", None], ["C", "A"]]

comments_1 = [["Bingo! Vašimi šaty okouzlíte snad každého muže v sále.",
               "Neuděláte vysloveně chybu. Ale večerní róba je prostě nejlepší.",
               "Šéf (a zdaleka nejen on) z Vás bude pohoršen, až Vás uvidí..."],
              ["Správně. Nyní jste připravená v kročit do víru noci.",
               "Není špatná volba. Ale otevřená špička není jednak praktická, ani nevypadá nejlépe.",
               "Žena s holýma nohama v tak vysoké společnosti. No nevím, nevím..."],
              ["Výborně. Jako první jste Vy představila svého muže. Pak je na řadě šéf, který představil svou choť.",
               None,
               "Špatně. Jako první jste Vy představila svého muže. Pak je na řadě šéf, který představí svou choť."],
              ["Zcela správně. Platí totiž základní pravidlo: příbory bereme z kraje směrem k talíři. Dezertní příbor "
               "je napříč nad talířem.",
                  None,
               "Faux pas! Platí totiž základní pravidlo: příbory bereme z kraje směrem k talíři.\n"
               "Dezertní příbor je napříč nad talířem."],
              ["Výborně. Máte plné právo odmítnout. Tím se ale zbavujete práva zatančit si následující tanec.",
               None,
               "Špatně. Máte totiž plné právo odmítnout, ale tím se zbavujete práva zatančit si následující tanec."]]
comments_2 = [["Smoking je trefa! Přesně jak bylo v pozvánce - Black-Tie.",
               "Šéf (a zdaleka nejen on) z Vás bude pohoršen, až Vás uvidí...",
               None],
              ["Správně. Tak to má být.",
               "Udělal jste jen malý přešlap, kterého si nejspíše okolí ani nevšimne.",
               "Cože?! Budete všem okolo přinejmenším pro smích."],
              ["Výborně. Jako první jste Vy představil svou ženu. Pak je na řadě šéf, který vám představil svou choť.",
               None,
               "Špatně. Jako první jste Vy představil svou ženu. Pak je na řadě šéf, který vám představil svou choť."],
              [comments_1[3][0],
               None,
               comments_1[3][2]],
              ["Bravo! Naprosto správně.",
               "Špatně. Byl jste sice pokorný. Nicméně šéf Vám neodpustí, že jste se jej nedotázal jako prvního, "
               "zda svolí svou choť k tanci.",
               "Naprostá hrůza! Váš šéf bude dlouho rozdýchávat Vaše chování."]]

assessment_1 = [[" Při volbě oblečení jste vůbec nezačala dobře. Málem Vás ani nechtěli pustit do sálu!\n",
                 " Příště volte slavnostnější oděv.\n",
                 " Začala jste naprosto správně. Večerní róbu Vám kdejaká žena mohla jen tiše závidět.\n"],
                ["Zapamatujte si: holé nohy = holé neštěstí!\n",
                 "Sandálky jste nezvolila špatně. Příště si ale vezměte raději lodičky.\n",
                 "S lodičkami jste opravdu okouzlujícím způsobem vplula do společnosti.\n"],
                ["S představováním máte problémy. Zkuste se podívat na Etiketu od pana Špačka.\n",
                 None,
                 "S představováním žádné problémy nemáte.\n"],
                ["Pokud není problém rozpoznat, k čemu jaký příbor je, tak si jen pamatujte:\n příbory vždy bereme z "
                 "vnějšku směrem k talíři.\n ",
                 None,
                 "U stolu můžete s klidem a náležitým prožitkem vychutnávat servírované pokrmy.\n"],
                ["Tančit možná umíte bravurně. Ale etiketu s tím spojenou ještě neovládáte.",
                 None,
                 "Nakonec jste až do hluboké noci tančila jako pírko."]]  # sada závěrečného hodnocení muži
assessment_2 = [[" Při volbě oblečení jste vůbec nezačal dobře. Málem Vás ani nechtěli pustit do sálu!\n",
                 " Příště lépe čtěte pozvánku!\n",
                 " Začal jste naprosto správně. Smoking Vám padl jako ulitý.\n"],
                ["Svršky napříště žádné raději nenoste, i kdyby venku mrzlo až praští, ať si neutrhnete zase ostudu.\n",
                 "Zapomněl jste, že nejprve pomůžeme dámě se svršky, pak teprve svlékneme své.\n",
                 "V šatně jste se nedopustil nejmenšího porušení etikety.\n"],
                ["S představováním máte problémy. Zkuste se podívat na Etiketu od pana Špačka.\n",
                 None,
                 "S představováním žádné problémy nemáte.\n"],
                ["Pokud není problém rozpoznat, k čemu jaký příbor je, tak si jen pamatujte:\n příbory vždy bereme z "
                 "vnějšku směrem k talíři.\n ",
                 None,
                 "U stolu můžete s klidem a náležitým prožitkem vychutnávat servírované pokrmy.\n"],
                ["U etikety tance jste naprosto propadl.",
                 "U etikety tance se máte ještě, co učit. Bude těžké Váš přešlap Vašemu šéfovi vysvětlovat.",
                 "Váš tanec i Vaše chování jsou v dokonalém souznění."]]  # sada závěrečného hodnocení ženy

points_1 = [[2, 1, 0], [2, 1, 0], [2, None, 0], [2, None, 0], [2, None, 0]]
points_2 = [[2, 0, 1], [2, 1, 0], [2, None, 0], [2, None, 0], [2, 1, 0]]
# vstupuje do f-ce ifs - přiděluje body podle zvolené možnosti

# --------------------------------------CODE----------------------------------------------------------------------------
# Instructions:
print("--INSTRUKCE/OVLÁDÁNÍ--".center(width))
print("Napište vždy zvolenou možnost a stiskněte enter.\n".center(width))
next()

# gender choice
gender = str(input("Chcete si zahrát jako žena (napište: žena) nebo jako muž (napište: muž)? "))

while gender != "žena" and gender != "muž":
    gender = input("Zadejte prosím slovo \"žena\" nebo \"muž\": ")
    continue  # kontrola vstupu - musí být žena nebo muž
if gender == "žena":
    gender = 1
elif gender == "muž":
    gender = 2

print("Vyčkejte prosím\n".center(width))
time.sleep(2)

# setting gender
questions = globals()["questions_{}".format(gender)]
assessment = globals()["assessment_{}".format(gender)]
sub_qns = globals()["sub_qns_{}".format(gender)]
answers = globals()["answers_{}".format(gender)]
comments = globals()["comments_{}".format(gender)]
points = globals()["points_{}".format(gender)]

# games's start
if gender == 1:
    print(wellcome[0])
elif gender == 2:
    print(wellcome[1])
print()
next()

# the Game
for i in range(5):
    print("\n", questions[i])
    time.sleep(4)

    choice = input(sub_qns[i])
    print()
    if gender == 2 and questions[0]:  # jediná otázka má 5 možných odpovědí
        input_correct("A", "B", "C", "D", "E", choice)
        comments[0][2] = ifs_in("B", "C", "E", choice)
    else:
        input_correct("A", "B", "C", "D", None, choice)
    ifs(answers[i][0], answers[i][1], comments[i][0], comments[i][1], comments[i][2], choice)
    print()
    next()

# overall assesment
print(assessment[0][behaviour[0]], assessment[1][behaviour[1]], assessment[2][behaviour[2]],
      assessment[3][behaviour[3]],
      assessment[4][behaviour[4]])
sum_behaviour = behaviour[0] + behaviour[1] + behaviour[2] + behaviour[3] + behaviour[4]
time.sleep(5)
print("\n\nCelkové hodnocení:\n")

# final assesment
if gender == 1:
    if sum_behaviour == 10:
        print("Večer byl par excellence! Šéf dokonce hovořil o Vašem povýšení. Jste zrozená pro společenské události.")
    elif behaviour[0] == 0:
        print(
            "Volbou oblečení jste si zkrátka zhatila večer. Šéf Vám druhý den vzkázal, že uvažuje o Vašem (minimálně) sesazení.")
    elif 9 >= sum_behaviour > 6:
        print("Strávila jste příjemný večer. Dopustila jste se drobných zaváhání, která Vám pro tentokráte společnost prominula.")
    elif 6 >= sum_behaviour > 3:
        print("Večer se Vám nevyvedl. Šéf Vám druhý den vzkázal, že uvažuje o Vašem (minimálně) sesazení.")
    else:
        print("Váš večer byla doslova katastrofa! Druhý den Vám ležela na stole výpověď od Vašeho šéfa.")
else:
    if sum_behaviour == 10:
        print(
            "Večer byl par excellence! Šéf dokonce hovořil o Vašem povýšení. Etiketa je přirozená součást Vašeho života.")
    elif behaviour[0] == 0:
        print(
            "Volbou oblečení jste si zkrátka zhatil večer. Šéf Vám druhý den vzkázal, že uvažuje o Vašem (minimálně) sesazení.")
    elif 9 >= sum_behaviour > 6:
        print("Prožil jste příjemný večer. Dopustil jste se sice nějakého přešlapu, ale společnost to tentokráte akceptovala.")
    elif 6 >= sum_behaviour > 3:
        print("Dopustil jste se významných přešlapů. To Vás může dokonce stát i místo!")
    else:
        print("Váš večer byl doslova katastrofa! Druhý den Vám ležela na stole výpověď od Vašeho šéfa.")


print("\n", "KONEC HRY".center(width))
