# Vytvorte pomoci for cyklu pyramidu z hvezdicek. Vystup bude vypadat nasledovne
#     *
#    ***
#   *****
#  *******
# *********
a = "*"
b = " "
star = 1
for i in range(80, 0, -1):
    print(i * b, star * a, sep="")
    star += 2