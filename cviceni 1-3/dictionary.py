# Vytvorte si seznam, ktere bude uvnitr obsahovat nekolik slovniku. Kazdy slovnik bude obsahovat 3 klice a 3 hodnoty
# (brand => Ford, engine => 1.0, price => 300000, percent=0.4).
# Takhle vytvorte alespon 3 slovniky se tremi druhy aut a rozdilnymi hodnotami.
# Pote iterujte prvni seznamem a uvnitr cyklu iterujte klicem i hodnotou jednotlivych slovniku.
# Pro kazdy automobil zvyšte cenu o procento, ktere se nachazi pod klicem percent
# Vysledne slovniky vypiste se zmenenymi hodnotami

list = [{"brand": "Ford", "engine": 1.0, "price": 300000, "percent" : 0.3}, {"brand": "Škoda", "engine": 1.3, "price": 400000, "percent" : 0.4},
          {"brand": "Ferrari", "engine": 2.8, "price": 5000000, "percent" : 0.2}]

for diction in list:
    diction["price"] = diction["price"] + diction["price"] * diction["percent"]
    print("Nová cena {}".format(diction["brand"]), "je", diction["price"])



#for i in range(3):
    #for (key, value) in seznam[i].items():
        #print(key, "->", value)