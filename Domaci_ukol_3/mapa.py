# 20 bodu
'''Kdo hral textovou hru od Googlu tak bude v obraze. Cilem bude udelat mapu. Muzete si udelat vlastni nebo pouzit moji.
Mapa se bude skládat z několika částí a bude reprezentována textem. Obrázek mapy se nachází pod tímto textem
Cilem toho cviceni bude spise vymyslet jak toto cviceni vyresit. Samotna implementace nebude tak narocna.

Prace s dvourozmernym polem je seznamem (polem) je jednoducha

pole = [['a','b','c','d'],
        ['e','f','g','h'],
        ['i','j','k','l']]

Dvourozmerne pole obsahuje radky a sloupce. Kazda hodnota ma svuj jedinecny index definovany indexem radku a sloupce
napr. pole[0][1] vybere hodnotu na prvni radku ve druhem sloupci 'b'.
      pole[1][3] vybere hodnotu na druhem radku ve ctvrtem sloupci 'h'.
Vice informaci najdete na internetu: https://www.tutorialspoint.com/python/python_2darray.htm

Mapa (dvourozmerne pole) predstavuje bludiste. Vase postava se na zacatku hry bude nachazet na pozici pismene S (index [7][0]).
Cílem je dostat se do cile, ktery oznacuje pismeno F (index [0][9]).
Budete se ptat uzivatele stejne jako v textove hre na informaci o tom kterym smerem chce jit - nahoru, doprava, dolu, doleva.
Po každém zadání posunu vykreslíte mapu, kde bude zobrazena pozice postavicky v mape a pozici v jejim primem okoli. Bude to vypadat asi takto:

      ?????
      ?***?
      ?*P.?
      ?*.*?
      ?????

Tečky představují cestu (místo kam může postavička stoupnout). Hvězdičky představují zeď, tam postavička nemůže vstoupit.
Ostatní okolí postavy (zbytek mapy) bude ukryt postavě a budou ho představovat otazníky.

# BONUS: 5 bodů
# Udělejte verzi, kde se bude ukládat a při vykreslování mapy zobrazovat i místo, které postava už prošla a tudíž ho zná.
'''

game_map = [['*', '*', '*', '*', '*', '*', '.', '.', '.', 'F'],
            ['*', '*', '*', '*', '.', '.', '.', '*', '*', '*'],
            ['.', '.', '.', '.', '.', '*', '*', '*', '*', '*'],
            ['.', '*', '*', '*', '*', '*', '*', '*', '.', '.'],
            ['.', '.', '.', '*', '*', '.', '.', '.', '.', '.'],
            ['*', '*', '.', '.', '.', '.', '*', '*', '*', '*'],
            ['*', '*', '.', '*', '*', '*', '*', '*', '*', '*'],
            ['S', '.', '.', '.', '.', '.', '.', '.', '.', '.']]


def game(state):
    # w-nahoru, a-doleva, s-dolů, d-doprava
    introduction()

    hidden_state = [["?" for j in range(len(state[0]))] for i in range(len(state))]
    position = [len(state) - 1, 0]

    while position != [0, len(state[0]) - 1]:
        # zjistím, kterými směry je možné jít
        free_directs = test_position(position, state)
        # vytisknu plán, s omezeným okolím position
        print_state(hidden_state, state, position)

        # získam žádanou pozici od hráče
        query = where_to_go(free_directs)
        input_query = input_position(query, free_directs)

        # změním pozici žádoucím směrem
        row, col = change_position(position, free_directs, input_query)
        position = [row, col]

        # opět zahalím pole otazníky
        hidden_state = [["?" for j in range(len(state[0]))] for i in range(len(state))]

    # nakonec vytisknu pole a pogratuluji
    print_state(hidden_state, state, position)
    print("Gratulujeme, jste v cíli!")
    return True


def test_position(position, state):
    free_directs = {"w": False, "s": False, "a": False, "d": False}

    row = position[0]
    col = position[1]

    row_limit = range(len(state))
    col_limit = range(len(state[0]))

    if row - 1 in row_limit and col in col_limit \
            and state[row - 1][col] in ".-F":
        free_directs["w"] = True

    if row + 1 in row_limit and col in col_limit \
            and state[row + 1][col] in ".-F":
        free_directs["s"] = True

    if col - 1 in col_limit and row in row_limit \
            and state[row][col - 1] in ".-F":
        free_directs["a"] = True

    if col + 1 in col_limit and row in row_limit \
            and state[row][col + 1] in ".-F":
        free_directs["d"] = True

    return free_directs


def print_state(hidden_state, state, position):
    for row in range(position[0] - 1, position[0] + 2):
        for col in range(position[1] - 1, position[1] + 2):
            if row == position[0] and col == position[1]:
                hidden_state[row][col] = "P"
                state[row][col] = "-"
            elif row in range(len(state)) and col in range(len(state[0])):
                hidden_state[row][col] = state[row][col]
    for row in hidden_state:
        print(row)
    return


def where_to_go(free_directs):
    directions = {"a": "doleva", "d": "doprava", "w": "nahoru", "s": "dolů"}
    possible_direct = []
    for direction in free_directs.keys():
        if free_directs[direction]:
            possible_direct.append(directions[direction])

    query = ""
    for i in range(len(possible_direct)):
        if i == len(possible_direct) - 2 and len(possible_direct) > 1:
            query = query + possible_direct[i] + " nebo "
        else:
            query = query + possible_direct[i] + ", "
    return query[:-2]


def input_position(query, free_directs):
    # pro překlad input_queries - zadá w - a zobrazím nahoru
    directions = {"a": "doleva", "d": "doprava", "w": "nahoru", "s": "dolů"}

    input_query = input("Chcete jít dále {}? ".format(query))

    while input_query not in "wasd" or not input_query:
        input_query = input("{} - Takový tah neznám!\n"
                            "Zadejte nový směr.".format(input_query))

    while not free_directs[input_query]:
        input_query = input("{} není možné jít! ".format(directions[input_query].capitalize()))

    return input_query


def change_position(position, free_directs, input_query):
    directs = {"w": [-1, 0], "a": [0, -1], "s": [1, 0], "d": [0, 1]}
    translate = directs[input_query]
    change_row = translate[0]
    change_col = translate[1]
    if free_directs[input_query]:
        new_position = [position[0] + change_row, position[1] + change_col]
        return new_position
    else:
        print("{} není možné jít! ".format(input_query))
    return False


def introduction():
    print("Vítejte ve hře Bludiště 1.0!\n"
          "Vašim úkolem je dojít do cíle - F.\n"
          "Vidíte ale vždy jen část hrací plochy.\n\n"
          "Hvězdička představuje stěnu - tam nemůžete jít.\n"
          "Tečka představuje volnou cestu.\n"
          "pomlčka značí místo, kudy jste již šli.\n"
          "Písmeno P značí Vaši aktuální pozici.\n\n"
          "Ovládání je následující:\n"
          "a - doleva, d - doprava, w - nahoru, s - dolů\n")

game(game_map)
