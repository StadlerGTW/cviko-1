import datetime
import time

# 1 bod
# z retezce string1 vyberte pouze prvni tri slova a pridejte za ne tri tecky. Vypiste je
# spravny vypis: There are more...
string1 = 'There are more things in Heaven and Earth, Horatio, than are dreamt of in your philosophy.'


def three_words(string):
    words = 0
    string_result = []
    for j in range(len(string)):
        if string[j] in " ,.":
            if not string[j - 1] in " .,":
                words += 1
        if words == 3:
            return "".join(string_result) + "..."
        string_result.append(string[j])


print(three_words(string1))


# 1 bod
# z retezce string1 vyberte pouze poslednich 11 znaku a vypiste je
# spravny vypis: philosophy.


def last_letters(string):
    new_string = ""
    for i in range(-11, 0, 1):
        new_string = new_string + string[i]
    return new_string


print(last_letters(string1))

# 1 bod
# retezec string2 rozdelte podle carek a jednotlive casti vypiste
# spravny vypis:
# I wondered if that was how forgiveness budded; not with the fanfare of epiphany
# but with pain gathering its things
# packing up
# and slipping away unannounced in the middle of the night.
string2 = 'I wondered if that was how forgiveness budded; not with the fanfare of epiphany, but with pain gathering its things, packing up, and slipping away unannounced in the middle of the night.'


def split_string(string):
    splited_string = string.split(", ")
    result = ""
    for part in splited_string:
        result = result + part + "\n"
    return result


print(split_string(string2))


# 2 body
# v retezci string2 spocitejte cetnost jednotlivych pismen a pote vypiste jednotlive cetnosti i pismenem
# spravny vypis: a: 12
#                b: 2
# atd.


def let_frequency(string):
    frequency = {}
    print_freq = []
    for letter in string.lower():
        if letter.isalpha():
            if letter in frequency:
                frequency[letter] += 1
            else:
                frequency[letter] = 1
    for key, value in frequency.items():
        print_freq.append([key, value])
    print_freq.sort(key=lambda x: x[0])

    for pair in print_freq:
        print(pair[0], ": ", pair[1])
    return


let_frequency(string2)

# 5 bodu
# retezec datetime1 predstavuje datum a cas ve formatu YYYYMMDDHHMMSS
# zparsujte dany retezec (rozdelte ho na rok, mesic, den, hodiny, minuty, sekundy a vytvorte z nej datum tak jak definuje
# funkce datetime. Pouzijete knihovnu a tridu datetime (https://docs.python.org/3/library/datetime.html#datetime.datetime)
# Potom slovy odpovezte na otazku: Co je to cas od epochy (UNIX epoch time)?
# Odpoved: Počet vteřin, které uběhly od půlnoci 1. 1. 1970. Je ukládán v 32bitovém celém čísle.
# Nasledne odectete zparsovany datum a cas od aktualniho casu (casu ziskaneho z pocitace pri zavolani jiste funkce)
# a vypocitejte kolik hodin cini rozdil a ten vypiste
datetime1 = '20181121191930'


def converter(input_time):
    conv = []
    cache = ""
    for i in range(4):
        cache = cache + input_time[i]
    conv.append(int(cache))
    for j in range(4, len(input_time) - 1, 2):
        cache = ""
        cache = cache + input_time[j] + input_time[j + 1]
        conv.append(int(cache))

    conv = datetime.datetime(conv[0], conv[1], conv[2], conv[3], conv[4], conv[5])

    # 2. část
    current_time = datetime.datetime.today()
    time_diff = current_time - conv
    time_diff = str(round(time_diff.total_seconds() / 3600, 5))
    time_diff = ",".join(time_diff.split("."))
    print("Rozdíl mezi aktuálním časem a daným časem {} je {} hodin.".format(conv, time_diff))

    return time_diff


converter(datetime1)


# 5 bodu
# v retezci string3 jsou slova prehazena. V promenne correct_string3 jsou slova spravne.
# vytvorte novou promennou, do ktere poskladate spravne retezec string3 podle retezce correct_string3
# nasledne vypocitej posun mist o ktere muselo byt slovo posunuto, aby bylo dano do spravne pozice.
# nereste pritom smer pohybu
# vypiste spravne poradi vety a jednotliva slova s jejich posunem
# napr. war: 8
#       the: 6
# atd.
string3 = 'war concerned itself with which Ministry The of Peace'
correct_string3 = 'The Ministry of Peace which concerned itself with war'


def correct_order(string, correct_string):
    words = string.split(" ")
    words_correct = correct_string.split(" ")
    len_cs = len(words_correct)

    correct_order = [[] for i in range(len_cs)]
    longest_word = len(words[0])

    for i in range(len(words_correct)):
        if len(words[i]) > longest_word:
            longest_word = len(words[i])
        j = 0
        while words[i] != words_correct[j] and j < len_cs:
            j += 1
        correct_order[j] = [words[i], i - j]

    for member in correct_order:
        print("{:<{}}".format(member[0], longest_word), ": ", "{:3d}".format(member[1]))
    return


correct_order(string3, correct_string3)
