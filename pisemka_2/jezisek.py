"""
Resenim teto pisemky bude zpracovat 3 dopisy (letter1, letter2 a letter3) od 3 deti, ktere pisou jeziskovi a nakoupit veci z dopisu.
V kazdem dopise je objekt, ktery je mozne koupit na eshopu (promenna stores).
Cilem je nakoupit jeziskovi v eshopech vsechny veci, o ktere si deti napsaly.
Jezisek ma omezeny rozpocet (promenna money), ale muze si vzit uver od banky (promenna bank).
Projdete dopisy deti a najdete v dopisech veci, ktere mate koupit. Veci jsou oddelene - (pomlckou) a jsou vzdy na konci vet.
V dopisech od deti jsou gramaticke chyby, takze bude potreba hledat varianty s i/y nebo ceske prepisy (pro plejstejsn budete hledat playstation).
Tyto veci se pote pokuste najit v eshopech a "koupit". Pri koupeni musite odecist jednu polozku z poctu dostupnych polozek na eshopu a odecist penize jeziska.
Preferujte nejlevnejsi moznost.
Pokud jezisek nebude mit na danou vec dost penez, bude si muset pujcit od banky (odectete penize z banky).
Nakonec programu vytvorite formatovany vypis do konzole, ktery bude obsahovat informace o vsech nakoupenych polozkach (obchod, cena).
Dale bude obsahovat kolik jeziskovy zbylo penez a kolik si pujcil od banky a o kolik vic zaplati na urocich.

SHRNUTI:
1. Zpracujete dopisy od deti (zohlednite gramaticke chyby a ceske tvary)
2. Vyhledate darky z dopisu na eshopu
3. Koupite nejlevnejsi darky na eshopech a odectete pocet polozek v obchode
4. Pokud jeziskovi nevyjdou penize tak si pujcite od banky
5. Vypisete informace o nakupech a jeziskovych financich

ZPUSOB IMPLEMENTACE JE NA VAS. SAMOTNE RESENI (FUNKCE) BUDETE MUSET VYMYSLET SAMI.
"""

bank = 1000000
rate = 19.9
money = 30000

letter1 = """Myly jezisku, na vanoce bych si pral -hodinky s vodotriskem"""
letter2 = """Na vanoce bich moc chtel -plejstejsn"""
letter3 = """Na vanoce bych si prala -ponika"""

stores = [
    {'name': 'Alza', 'products': [
        {'name': 'hodinky s vodotryskem', 'price': 150, 'no_items': 5},
        {'name': 'pocitac', 'price': 20000, 'no_items': 2},
        {'name': 'ponik', 'price': 50000, 'no_items': 1}
    ]},
    {'name': 'CZC', 'products': [
        {'name': 'playstation', 'price': 9000, 'no_items': 5},
        {'name': 'Dobroty Ladi Hrusky', 'price': 10, 'no_items': 100},
        {'name': 'ponik', 'price': 70000, 'no_items': 1}
    ]}
]


def item_wished(*args):
    wishes = []
    for item in args:
        list_of_let = list(item)
        wish = ""
        for i in range(len(list_of_let)):
            if list_of_let[i] in "-":
                index = i + 1
                for j in range(index, len(list_of_let)):
                    wish += list_of_let[j]
                wishes.append(wish)

    wishes_correct = translate(wishes)
    return wishes_correct


def translate(list_of_wishes):
    dict_wishes = {"hodinky s vodotriskem": "hodinky s vodotryskem", "plejstejsn": "playstation", "ponika": "ponik"}
    for i in range(len(list_of_wishes)):
        if list_of_wishes[i] in dict_wishes:
            list_of_wishes[i] = dict_wishes[list_of_wishes[i]]
    return list_of_wishes


wishes = item_wished(letter1, letter2, letter3)


def buy(wishes, money, bank):
    result = []
    for wish in wishes:
        temp_price = 1000000
        for store in stores:
            for product in store["products"]:
                if wish == product["name"] and product["price"] < temp_price:
                    temp_price = product["price"]
                    shop_index = stores.index(store)
        if money > temp_price:
            money -= temp_price
        else:
            bank -= temp_price
        result.append([wish, stores[shop_index]["name"], temp_price])
        for i in range(len(stores[shop_index]["products"])):
            if wish == stores[shop_index]["products"][i]["name"]:
                stores[shop_index]["products"][i]["no_items"] = stores[shop_index]["products"][i].get("no_items", 0) - 1

    print("Ježíšek koupil:")

    i = 1
    for item in result:
        if i == len(result):
            print("a {} v {} za {} Kč.\n".format(item[0], item[1], item[2]), end="")
        else:
            print("{} v {} za {} Kč,\n".format(item[0], item[1], item[2]), end="")
        i += 1
    print("V bance si půjčil {} Kč a zaplatil ze svého {}.".format(bank, money))


buy(wishes, money, bank)
